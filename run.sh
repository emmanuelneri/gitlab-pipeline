#!/usr/bin/env bash

mvn clean package

docker build -t backend/app .

docker run -d \
    --name backend \
    -p 8090:8090 \
    backend/app