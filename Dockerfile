FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/gitlab-pipeline-*.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]